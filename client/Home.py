import streamlit as st

st.set_page_config(
    page_title="SPTFY",
    page_icon="🎵",
)

st.sidebar.success("Select any of the feature.")

st.markdown(
    """
    # Spotify Music Discovery Project 𝄞   

    ## Overview

    We're analyzing Spotify listening data to understand user behavior and preferences, aiming to enhance music discovery through personalized playlists.

    """
)